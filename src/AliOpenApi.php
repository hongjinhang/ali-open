<?php
namespace Alibaba\OpenApi;

use Alibaba\OpenApi\Core\ContainerBase;
use Alibaba\OpenApi\Library\Order;
use Alibaba\OpenApi\Library\Refund;
use Alibaba\OpenApi\Library\Logistics;
use Alibaba\OpenApi\Provider\OrderProvider;
use Alibaba\OpenApi\Provider\RefundProvider;
use Alibaba\OpenApi\Provider\LogisticsProvider;
use Alibaba\OpenApi\Provider\ProductProvider;

/**
 * Class Application
 * @property Order order
 * @property Goods goods
 */
class AliOpenApi extends ContainerBase
{
    /**
     * 服务提供者
     * @var array
     */
    public function __construct($params = [])
    {
        parent::__construct($params);
    }

    /**
     * 其他服务提供者
     * @var string[]
     */
    protected $provider = [
        OrderProvider::class,
        RefundProvider::class,
        LogisticsProvider::class,
        ProductProvider::class,
    ];

}