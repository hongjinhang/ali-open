<?php
namespace Alibaba\OpenApi\Library;

use Alibaba\OpenApi\Core\BaseClient;

class Order extends BaseClient
{
    /**
     * [fastCreateOrder 创建采购订单]
     * @return [type] [description]
     */
    public function fastCreateOrder(): Order
    {
        return $this->setApi('com.alibaba.trade:alibaba.trade.fastCreateOrder-1');
    }

    /**
     * [getReceiveAddress 买家获取保存的收货地址信息列表]
     * @return [type] [description]
     */
    public function getReceiveAddress(): Order
    {
        return $this->setApi('com.alibaba.trade:alibaba.trade.receiveAddress.get-1');
    }

    /**
     * [cancel 取消交易]
     * @return [type] [description]
     */
    public function cancel(): Order
    {
        return $this->setApi('com.alibaba.trade:alibaba.trade.cancel-1');
    }

    /**
     * [getAlpayUrl 批量获取订单的支付链接]
     * @return [type] [description]
     */
    public function getAlpayUrl(): Order
    {
        return $this->setApi('com.alibaba.trade:alibaba.alipay.url.get-1');
    }

    /**
     * [memoAdd 修改订单备忘]
     * @return [type] [description]
     */
    public function memoAdd(): Order
    {
        return $this->setApi('com.alibaba.trade:alibaba.order.memoAdd-1');
    }

    /**
     * [getDetail 订单详情查看(买家视角)]
     * @return [type] [description]
     */
    public function getDetail() : Order
    {
        return $this->setApi('com.alibaba.trade:alibaba.trade.get.buyerView-1');
    }

    /**
     * [getBuyerOrderList 订单列表查看(买家视角)]
     * @return [type] [description]
     */
    public function getBuyerOrderList() : Order
    {
        return $this->setApi('com.alibaba.trade:alibaba.trade.getBuyerOrderList-1');
    }

}
