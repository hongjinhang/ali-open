<?php
namespace Alibaba\OpenApi\Library;

use Alibaba\OpenApi\Core\BaseClient;

class Refund extends BaseClient
{
    /**
     * [getRefundReasonList 查询退款退货原因（用于创建退款退货）]
     * @return [type] [description]
     */
    public function getRefundReasonList(): Refund
    {
        return $this->setApi('com.alibaba.trade:alibaba.trade.getRefundReasonList-1');
    }

    /**
     * [uploadRefundVoucher ]
     * @return [type] [description]
     */
    public function uploadRefundVoucher(): Refund
    {
        return $this->setApi('com.alibaba.trade:alibaba.trade.uploadRefundVoucher-1');
    }

    /**
     * [createRefund 创建退款退货申请]
     * @return [type] [description]
     */
    public function createRefund(): Refund
    {
        return $this->setApi('com.alibaba.trade:alibaba.trade.createRefund-1');
    }

    /**
     * [queryOrderRefundList 查询退款单列表(买家视角)]
     * @return [type] [description]
     */
    public function queryOrderRefundList(): Refund
    {
        return $this->setApi('com.alibaba.trade:alibaba.trade.refund.buyer.queryOrderRefundList-1');
    }

    /**
     * [OpQueryBatchRefundByOrderIdAndStatus 查询退款单详情-根据订单ID]
     */
    public function OpQueryBatchRefundByOrderIdAndStatus(): Refund
    {
        return $this->setApi('com.alibaba.trade:alibaba.trade.refund.OpQueryBatchRefundByOrderIdAndStatus-1');
    }

    /**
     * [OpQueryOrderRefund 查询退款单详情-根据退款单ID]
     */
    public function OpQueryOrderRefund(): Refund
    {
        return $this->setApi('com.alibaba.trade:alibaba.trade.refund.OpQueryOrderRefund-1');
    }

    /**
     * [returnGoods 买家提交退款货信息]
     * @return [type] [description]
     */
    public function returnGoods(): Refund
    {
        return $this->setApi('com.alibaba.trade:alibaba.trade.refund.returnGoods-1');
    }

}
