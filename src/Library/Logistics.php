<?php
namespace Alibaba\OpenApi\Library;

use Alibaba\OpenApi\Core\BaseClient;

class Logistics extends BaseClient
{
    /**
     * [getLogisticsInfos 获取交易订单的物流信息(买家视角)]
     * @return [type] [description]
     */
    public function getLogisticsInfos(): Logistics
    {
        return $this->setApi('com.alibaba.logistics:alibaba.trade.getLogisticsInfos.buyerView-1');
    }

    /**
     * [getLogisticsTraceInfo 获取交易订单的物流跟踪信息(买家视角)]
     * @return [type] [description]
     */
    public function getLogisticsTraceInfo(): Logistics
    {
        return $this->setApi('com.alibaba.logistics:alibaba.trade.getLogisticsTraceInfo.buyerView-1');
    }

}
