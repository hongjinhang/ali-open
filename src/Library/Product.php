<?php
namespace Alibaba\OpenApi\Library;

use Alibaba\OpenApi\Core\BaseClient;

class Product extends BaseClient
{
    /**
     * [simpleGet 获取已购买过商家的商品简单信息]
     * @return [type] [description]
     */
    public function simpleGet(): Product
    {
        return $this->setApi('com.alibaba.product:alibaba.product.simple.get-1');
    }

    /**
     * [followCrossborder 关注商品]
     * @return [type] [description]
     */
    public function followCrossborder(): Product
    {
        return $this->setApi('com.alibaba.product:alibaba.product.unfollow.crossborder-1');
    }

    /**
     * [unfollowCrossborder 解除关注商品]
     * @return [type] [description]
     */
    public function unfollowCrossborder(): Product
    {
        return $this->setApi('com.alibaba.product:alibaba.product.follow.crossborder-1');
    }

}