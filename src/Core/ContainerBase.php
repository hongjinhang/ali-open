<?php

namespace Alibaba\OpenApi\Core;

/**
 * 处理主类
 */
class ContainerBase extends Container
{
    protected $provider = [];

    public $params = [];

    public $baseUrl = 'https://gw.open.1688.com/openapi/';

    public $appKey = '';

    public $appSecret = '';

    public $accessToken = '';

    /**
     * 构造方法
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->params = $params;

        $provider_callback = function ($provider) {
            $obj = new $provider;
            $this->serviceRegister($obj);
        };
        //注册服务者
        array_walk($this->provider, $provider_callback);
    }

    /**
     * 获取器
     * @param $id
     * @return mixed
     */
    public function __get($id)
    {
        return $this->offsetGet($id);
    }

    /**
     * App Secret
     * @param $appSecret
     */
    public function setAppSecret($appSecret)
    {
        $this->appSecret = $appSecret;

    }

    /**
     * App Key
     * @param $appKey
     */
    public function setAppKey($appKey)
    {
        $this->appKey = $appKey;

        return $this;
    }

    /**
     * access Token
     * @param $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * BaseUrl
     * @param $BaseUrl
     */
    public function setBaseUrl($BaseUrl)
    {
        $this->baseUrl = $BaseUrl;

        return $this;
    }

    /**
     * params
     * @param $params
     */
    public function setParams(array $params = [])
    {
        $this->params = $params;

        return $this;
    }
}
