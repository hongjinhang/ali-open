<?php
namespace Alibaba\OpenApi\Provider;


use Alibaba\OpenApi\Core\Container;
use Alibaba\OpenApi\Library\Product;
use Alibaba\OpenApi\Interfaces\Provider;

/**
 * 获取商品信息
 */
class ProductProvider implements Provider
{
    public function serviceProvider(Container $container)
    {
        $container['product'] = function ($container){
            return new Product($container);
        };
    }
}