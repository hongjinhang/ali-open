<?php
namespace Alibaba\OpenApi\Provider;


use Alibaba\OpenApi\Core\Container;
use Alibaba\OpenApi\Library\Logistics;
use Alibaba\OpenApi\Interfaces\Provider;

/**
 * 物流服务提供者
 */
class LogisticsProvider implements Provider
{
    public function serviceProvider(Container $container)
    {
        $container['logistics'] = function ($container){
            return new Logistics($container);
        };
    }
}