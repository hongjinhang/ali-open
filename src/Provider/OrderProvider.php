<?php
namespace Alibaba\OpenApi\Provider;


use Alibaba\OpenApi\Core\Container;
use Alibaba\OpenApi\Library\Order;
use Alibaba\OpenApi\Interfaces\Provider;

/**
 * 订单服务提供者
 */
class OrderProvider implements Provider
{
    public function serviceProvider(Container $container)
    {
        $container['order'] = function ($container){
            return new Order($container);
        };
    }
}