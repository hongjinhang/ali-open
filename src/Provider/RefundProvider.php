<?php
namespace Alibaba\OpenApi\Provider;


use Alibaba\OpenApi\Core\Container;
use Alibaba\OpenApi\Library\Refund;
use Alibaba\OpenApi\Interfaces\Provider;

/**
 * 退款售后服务提供者
 */
class RefundProvider implements Provider
{
    public function serviceProvider(Container $container)
    {
        $container['refund'] = function ($container){
            return new Refund($container);
        };
    }
}