<?php
namespace Alibaba\OpenApi\Interfaces;

use Alibaba\OpenApi\Core\Container;

interface Provider
{
    public function serviceProvider(Container $container);
}